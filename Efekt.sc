Efekt {
	*new{ arg effect;
		Reagent.activeEffect_(effect);
		effect.();
		Reagent.activeEffect_(nil);
	}
}