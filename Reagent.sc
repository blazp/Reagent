Reagent : Ref {
	classvar >activeEffect;
	var targetMap;

	*new { arg initialValue;
		^super.new.init.value_(initialValue);
	}
	init {
		targetMap = Dictionary(100);
	}

	value {
		this.track(this)
		^value
	}

	value_ { |newValue|
		var oldValue = value;
		value = newValue;
		if (oldValue != value) { this.trigger(this) } {};
	}

	track { arg target;
		if ( activeEffect.notNil )
		{
			var deps = targetMap.at(target);
			if(deps.isNil,{	deps = IdentitySet[];	},	{});
			deps.add(activeEffect);
			targetMap.put(target,deps);
		} {}
	}

	trigger { arg target;
		var deps = targetMap.at(target);
		if( deps.notNil ) { deps.do({ arg effect; effect.(); }) } {}
	}

	test {
		^targetMap;
	}
}